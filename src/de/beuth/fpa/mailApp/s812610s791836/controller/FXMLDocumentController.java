package de.beuth.fpa.mailApp.s812610s791836.controller;

import de.beuth.fpa.mailApp.s812610s791836.applicationLogic.ApplicationLogicIF;
import de.beuth.fpa.mailApp.s812610s791836.applicationLogic.data.Component;
import de.beuth.fpa.mailApp.s812610s791836.applicationLogic.data.Email;
import de.beuth.fpa.mailApp.s812610s791836.applicationLogic.data.Email.Importance;
import de.beuth.fpa.mailApp.s812610s791836.applicationLogic.data.Folder;
import de.beuth.fpa.mailApp.s812610s791836.applicationLogic.MailerLogic;
import de.beuth.fpa.mailApp.s812610s791836.applicationLogic.data.Paths;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeItem.TreeModificationEvent;
import javafx.scene.control.TreeView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * This is the controller that is responsible for the main window of the
 * MailApp.
 *
 * @author Thesa
 */
public class FXMLDocumentController implements Initializable {

    /**
     * The labels of menu buttons
     */
    private static final String SAVE = "save";
    private static final String HISTORY = "history";
    private static final String OPEN = "open";
    private static final String NEW_ACCOUNT = "new account";
    // The image for the folder icon
    private Image folderIcon;
    // The image for the expanded folder icon
    private Image folderOpenIcon;
    // The image for the file icon
    private Image fileIcon;
    // The facade for the application logic
    private ApplicationLogicIF logic;
    // The List that holds the history of base directories
    private ArrayList<String> history;
    // The FXMLController which is responsible for the base directory history window.
    private HistoryFrameController historyController;
    // The FXMLController which is responsible for the new / edit account window.
    private EditAccountFrameController editAccountController;
    // The TreeView that illustrates the folder hierarchy
    @FXML
    private TreeView treeView;
    // The MenuBar of the main window
    @FXML
    private MenuBar menuBar;
    // The "Open Account" Menu
    @FXML
    private Menu openAccountMenu;
    // The "Edit Account" Menu
    @FXML
    private Menu editAccountMenu;
    // The tableview for emails of the selected folder
    @FXML
    private TableView emailTable;
    // The coloumn "Received", used to sort the table
    private TableColumn receivedCol;
    // The label for sender information
    @FXML
    private Label senderLabel;
    // The label for subject information
    @FXML
    private Label subjectLabel;
    // The label for received information
    @FXML
    private Label receivedLabel;
    // The label for receiver information
    @FXML
    private Label receiverLabel;
    // The textarea for displaying the email text
    @FXML
    private TextArea emailTextArea;
    // The search field above the email table
    @FXML
    private TextField searchField;
    // The label right of the search field
    @FXML
    private Label resultCountLabel;

    @Override
    public void initialize(final URL url, final ResourceBundle rb) {
        logic = new MailerLogic(new File(Paths.ROOT_PATH));
        historyController = new HistoryFrameController(logic, this);
        editAccountController = new EditAccountFrameController(logic, this);
        history = new ArrayList<>();
        try {
            folderIcon = new Image(getClass().getResourceAsStream(Paths.FOLDER_ICON_PATH));
            folderOpenIcon = new Image(getClass().getResourceAsStream(Paths.FOLDER_GREY_ICON_PATH));
            fileIcon = new Image(getClass().getResourceAsStream(Paths.FILE_ICON_PATH));
        } catch (final NullPointerException e) {
            System.out.println("Konnte Icons nicht laden");
        }
        configureTree();
        configureMenu();
        configureTable();
        configureSearch();
    }

    /**
     * This method creates the root TreeItem for the TreeView, sets the methods
     * that should be called after an event in the TreeView via Lambda
     * expressions and calls another method to load its content.
     */
    public void configureTree() {
        treeView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        final Folder topFolder = logic.getTopFolder();
        final TreeItem<Component> rootItem = new TreeItem(topFolder, new ImageView(folderOpenIcon));
        rootItem.setExpanded(true);
        rootItem.addEventHandler(TreeItem.branchExpandedEvent(), (event) -> this.onExpanded((TreeModificationEvent) event));
        rootItem.addEventHandler(TreeItem.branchCollapsedEvent(), (event) -> this.onCollapsed((TreeModificationEvent) event));
        showFolders(rootItem);
        treeView.getSelectionModel().selectedItemProperty().addListener((ObservableValue observable, Object oldValue, Object newValue) -> onFolderSelected(observable, oldValue, newValue));
        treeView.setRoot(rootItem);
    }

    /**
     * This method sets the event handlers for all MenuItems in the MenuBar.
     */
    private void configureMenu() {
        updateAccountMenu();
        final List<Menu> menus = menuBar.getMenus();
        for (final Menu menu : menus) {
            for (final MenuItem item : menu.getItems()) {
                item.setOnAction((event) -> this.handleMenuEvent(event));
            }
        }
    }

    /**
     * This method creates MenuItems for each account and adds them to the "Open
     * Account" and "Edit Account" menus.
     */
    public void updateAccountMenu() {
        openAccountMenu.getItems().clear();
        editAccountMenu.getItems().clear();
        List<String> accounts = logic.getAllAccounts();
        for (String name : accounts) {
            MenuItem menuItem = new MenuItem(name);
            openAccountMenu.getItems().add(menuItem);
            menuItem.setOnAction((event) -> this.handleOpenAccountMenuEvent(event));
            menuItem = new MenuItem(name);
            editAccountMenu.getItems().add(menuItem);
            menuItem.setOnAction((event) -> this.handleEditAccountMenuEvent(event));
        }
    }

    /**
     * This method creates a TreeItem for each component inside the folder.
     *
     * @param treeItem the folder as TreeItem
     */
    public void showFolders(final TreeItem<Component> treeItem) {
        if (!treeItem.getChildren().isEmpty()) {
            return;
        }
        final Component parent = (Component) treeItem.getValue();
        final List<Component> comps = parent.getComponents();
        for (final Component comp : comps) {
            final TreeItem<Component> item;
            if (comp instanceof Folder) {
                item = new ComponentTreeItem(comp, new ImageView(folderIcon));
            } else {
                item = new ComponentTreeItem(comp, new ImageView(fileIcon));
            }
            treeItem.getChildren().add(item);
        }
    }

    /**
     * This method gets called when a TreeItem gets selected. It prints the
     * emails in the folder to System.out.
     *
     * @param observable
     * @param oldValue The item that was selected before.
     * @param newValue The item that is now selected.
     */
    private void onFolderSelected(final ObservableValue observable, final Object oldValue, final Object newValue) {
        if (newValue == null) { //preventing NullPointerException at root directory change
            return;
        }
        final TreeItem<Component> selectedItem = (TreeItem<Component>) newValue;
        final Folder folder = (Folder) selectedItem.getValue();
        logic.loadEmails(folder);

        // force updating the displayed name
        selectedItem.setValue(null);
        selectedItem.setValue(folder);

        /**
         * for (final Email email : folder.getEmails()) {
         * System.out.println(email.toString());
        }
         */
        clearEmailArea();
        final String searchQuery = searchField.getText();
        showFilteredEmails(searchQuery);
        emailTable.getSortOrder().add(receivedCol);
    }

    /**
     * This method clears the email information in the labels.
     */
    private void clearEmailArea() {
        senderLabel.setText("---");
        subjectLabel.setText("---");
        receivedLabel.setText("---");
        receiverLabel.setText("---");
        emailTextArea.setText("");
    }

    /**
     * This method sets the information in the labels for the selected email. It
     * gets called when one email in the TableView is selected.
     *
     * @param email the selected email
     */
    private void setEmailArea(final Email email) {
        senderLabel.setText(email.getSender());
        subjectLabel.setText(email.getSubject());
        receivedLabel.setText(email.getReceived());
        receiverLabel.setText(email.getReceiver());
        emailTextArea.setText(email.getText());
    }

    /**
     * This method gets called when a TreeItem gets expanded. It calls another
     * method to load the content of the folder and to create the corresponding
     * TreeItems. It also changes the icon of the TreeItem to an open folder.
     *
     * @param event the TreeModificationEvent that gets fired when a TreeItem is
     * expanded
     */
    private void onExpanded(final TreeModificationEvent event) {
        final TreeItem item = event.getTreeItem();
        logic.loadContent((Folder) item.getValue());
        item.setGraphic(new ImageView(folderOpenIcon));
        showFolders(item);
    }

    /**
     * This method gets called when a TreeItem gets collapsed. It changes the
     * icon of the TreeItem to a closed folder.
     *
     * @param event the TreeModificationEvent that gets fired when a TreeItem is
     * collapsed
     */
    private void onCollapsed(final TreeModificationEvent event) {
        final TreeItem item = event.getTreeItem();
        item.setGraphic(new ImageView(folderIcon));
    }

    /**
     * This method opens a dialog for selecting the root path of the TreeView.
     * It changes the root folder and updates the TreeView.
     */
    private void showChangeRootDialog() {
        final DirectoryChooser dirChooser = new DirectoryChooser();
        dirChooser.setTitle("Change Root Directory");
        final File dir = dirChooser.showDialog(null);
        if (dir != null) {
            history.add(dir.getPath());
            logic.changeDirectory(dir);
            configureTree();
        }
    }

    /**
     * This method opens a dialog for selecting the directory for saving the
     * emails of the selected folder.
     */
    private void showSaveDialog() {
        final DirectoryChooser dirChooser = new DirectoryChooser();
        dirChooser.setTitle("Save Emails");
        final File dir = dirChooser.showDialog(null);
        if (dir != null) {
            final TreeItem item = (TreeItem) treeView.getSelectionModel().getSelectedItem();
            final Folder folder = (Folder) item.getValue();
            logic.saveEmails(dir, folder);
        }
    }

    /**
     * This method creates a new dialog which contains a ListView with the
     * history of the previous selected base directories. For loading the
     * history in the ListView another method is called.
     */
    private void showHistoryFrame() {
        final Stage historyStage = new Stage(StageStyle.UTILITY);
        historyStage.setTitle("Base Directory History");
        final URL location = getClass().getResource("/de/beuth/fpa/mailApp/s812610s791836/view/HistoryFrame.fxml");

        final FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(location);
        fxmlLoader.setController(historyController);

        try {
            final Pane myPane = (Pane) fxmlLoader.load();
            final Scene myScene = new Scene(myPane);
            historyStage.setScene(myScene);
            historyStage.show();
            historyController.setUpHistory(history);
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * This method creates a new dialog where the use can create a new account.
     */
    private void showNewAccountFrame() {
        final Stage editAccountStage = new Stage(StageStyle.UTILITY);
        editAccountStage.setTitle("New Account");
        final URL location = getClass().getResource("/de/beuth/fpa/mailApp/s812610s791836/view/EditAccountFrame.fxml");

        final FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(location);
        fxmlLoader.setController(editAccountController);

        try {
            final Pane myPane = (Pane) fxmlLoader.load();
            final Scene myScene = new Scene(myPane);
            editAccountStage.setScene(myScene);
            editAccountStage.show();
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * This method handles the events of the menu items.
     *
     * @param event the triggered event
     */
    private void handleMenuEvent(final Event event) {
        final Object source = event.getSource();
        final String text = ((MenuItem) source).getText().toLowerCase();
        switch (text) {
            case OPEN:
                showChangeRootDialog();
                break;
            case HISTORY:
                showHistoryFrame();
                break;
            case SAVE:
                showSaveDialog();
                break;
            case NEW_ACCOUNT:
                showNewAccountFrame();
                break;
            default:
                break;
        }
    }

    /**
     * This method handles the events of the "Open Account" menu items.
     *
     * @param event the triggered event
     */
    private void handleOpenAccountMenuEvent(final Event event) {
        final Object source = event.getSource();
        final String name = ((MenuItem) source).getText();
        logic.openAccount(name);
        configureTree();
    }

    /**
     * This method handles the events of the "Edit Account" menu items.
     *
     * @param event the triggered event
     */
    private void handleEditAccountMenuEvent(final Event event) {
        final Object source = event.getSource();
        final String name = ((MenuItem) source).getText();
        showNewAccountFrame();
        editAccountController.changeToEdit(name);
    }

    /**
     * This method sets up the tableview for the emails of the selected folder.
     */
    private void configureTable() {
        emailTable.setEditable(true);
        emailTable.setPlaceholder(new Label("Keine Emails in diesem Ordner"));

        final TableColumn importanceCol = new TableColumn("Importance");
        importanceCol.setMinWidth(100);
        importanceCol.setCellValueFactory(new PropertyValueFactory<Email, Importance>("importance"));

        receivedCol = new TableColumn("Received");
        receivedCol.setMinWidth(200);
        receivedCol.setCellValueFactory(new PropertyValueFactory<Email, String>("received"));
        receivedCol.setComparator((Object s1, Object s2) -> {
            try {
                String st1 = (String) s1;
                String st2 = (String) s2;
                final DateFormat format = DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.SHORT, Locale.GERMANY);
                final Date d1 = format.parse(st1);
                final Date d2 = format.parse(st2);
                return d1.compareTo(d2);
            } catch (final ParseException p) {
                p.printStackTrace();
            }
            return -1;
        });
        receivedCol.setSortType(TableColumn.SortType.DESCENDING);

        final TableColumn readCol = new TableColumn("Read");
        readCol.setMinWidth(50);
        readCol.setCellValueFactory(
                new PropertyValueFactory<Email, Boolean>("read"));

        final TableColumn senderCol = new TableColumn("Sender");
        senderCol.setMinWidth(100);
        senderCol.setCellValueFactory(
                new PropertyValueFactory<Email, String>("sender"));

        final TableColumn recipientsCol = new TableColumn("Recipients");
        recipientsCol.setMinWidth(100);
        recipientsCol.setCellValueFactory(
                new PropertyValueFactory<Email, List>("receiverTo"));

        final TableColumn subjectCol = new TableColumn("Subject");
        subjectCol.setMinWidth(100);
        subjectCol.setCellValueFactory(
                new PropertyValueFactory<Email, String>("subject"));

        emailTable.getColumns().addAll(importanceCol, receivedCol, readCol, senderCol, recipientsCol, subjectCol);
        emailTable.getSortOrder().add(receivedCol);
        emailTable.getSelectionModel().selectedItemProperty().addListener((ObservableValue observable, Object oldValue, Object newValue) -> onEmailSelected(newValue));
    }

    /**
     * This method gets called when one email in the TableView is selected. It
     * displays the email text in the textarea.
     *
     * @param observable
     * @param oldValue the item that was selected before
     * @param newValue the item that is now selected
     */
    private void onEmailSelected(final Object newValue) {
        if (newValue == null) {
            return;
        }
        final Email email = (Email) newValue;
        setEmailArea(email);
    }

    /**
     * Sets the event handler for changes in the search field.
     */
    private void configureSearch() {
        searchField.textProperty().addListener((observable, oldValue, newValue) -> onSearchFieldChange(newValue));
    }

    /**
     * This method gets called when the text inside the search field changes. It
     * display the emails that matches the pattern in the search field.
     *
     * @param newValue
     */
    private void onSearchFieldChange(final String newValue) {
        TreeItem<Component> item = (TreeItem<Component>) treeView.getSelectionModel().getSelectedItem();
        Folder folder = (Folder) item.getValue();
        showFilteredEmails(newValue);
    }

    /**
     * Displays only the emails in a folder that matches a pattern. It also
     * shows the number of these emails next to the search field.
     *
     * @param folder The folder that contains the emails.
     * @param pattern The pattern that has to be matched.
     */
    private void showFilteredEmails(final String pattern) {
        final List<Email> result = logic.searchEmails(pattern);
        emailTable.setItems(FXCollections.observableArrayList(result));
        if (pattern.isEmpty()) {
            resultCountLabel.setText("---");
        } else {
            resultCountLabel.setText(String.format("(%d)", result.size()));
        }
    }
}
