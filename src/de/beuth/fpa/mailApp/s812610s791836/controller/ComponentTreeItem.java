package de.beuth.fpa.mailApp.s812610s791836.controller;

import de.beuth.fpa.mailApp.s812610s791836.applicationLogic.data.Component;
import javafx.scene.Node;
import javafx.scene.control.TreeItem;

/**
 *
 *
 * @author Tim
 */
public class ComponentTreeItem extends TreeItem<Component> {

    public ComponentTreeItem() {
    }

    public ComponentTreeItem(Component value) {
        super(value);
    }

    public ComponentTreeItem(Component value, Node graphic) {
        super(value, graphic);
    }

    @Override
    public boolean isLeaf() {
        return !getValue().isExpandable();
    }

}
