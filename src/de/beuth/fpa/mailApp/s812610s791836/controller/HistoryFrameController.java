package de.beuth.fpa.mailApp.s812610s791836.controller;

import de.beuth.fpa.mailApp.s812610s791836.applicationLogic.ApplicationLogicIF;
import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.stage.Stage;

/**
 * History Frame Controller class
 *
 * @author Thesa
 */
public class HistoryFrameController implements Initializable {

    // The OK Button
    @FXML
    private Button okButton;
    // The Cancel Button
    @FXML
    private Button cancelButton;
    // The ListView that contains the history
    @FXML
    private ListView historyList;
    // The FXMLDocumentController of the main frame
    private final FXMLDocumentController fxmlcontr;
    private final ApplicationLogicIF logic;

    /**
     * The constructor of the HistoryFrameController which initializes the
     * constructor of the main frame
     *
     * @param logic - the application logic used by the main frame
     * @param fxmlcontr - the controller of the main frame
     */
    public HistoryFrameController(final ApplicationLogicIF logic, FXMLDocumentController fxmlcontr) {
        this.logic = logic;
        this.fxmlcontr = fxmlcontr;
    }

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(final URL url, final ResourceBundle rb) {
        okButton.setOnAction((event) -> pressedOK());
        cancelButton.setOnAction((event) -> pressedCancel());
    }

    /**
     * This method closes the window.
     *
     * @param button button which was pushed
     */
    public void close(final Button button) {
        Stage window = (Stage) button.getScene().getWindow();
        window.close();
    }

    /**
     * This method fills the ListView with the history of base directories. If
     * no base directories were selected before it shows a message that no base
     * direcories are in the history.
     *
     * @param history list with base directory history
     */
    public void setUpHistory(final List<String> history) {
        if (history.isEmpty()) {
            final ObservableList<String> empty = FXCollections.observableArrayList();
            empty.add("No base directories in history");
            historyList.setItems(empty);
        } else {
            final ObservableList<String> list = FXCollections.observableArrayList(history);
            historyList.setItems(list);
        }
    }

    /**
     * This method gets called when the ok button is pushed.
     */
    private void pressedOK() {
        final File path = new File((String) historyList.getSelectionModel().getSelectedItem());
        logic.changeDirectory(path);
        fxmlcontr.configureTree();
        close(okButton);
    }

    /**
     * This method gets called when the cancel button is pushed.
     */
    private void pressedCancel() {
        close(okButton);
    }

}
