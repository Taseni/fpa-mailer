package de.beuth.fpa.mailApp.s812610s791836.controller;

import de.beuth.fpa.mailApp.s812610s791836.applicationLogic.ApplicationLogicIF;
import de.beuth.fpa.mailApp.s812610s791836.applicationLogic.data.Account;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * Edit Account Frame Controller class
 *
 * @author Tim
 */
public class EditAccountFrameController implements Initializable {

    static final String UPDATE_BUTTON_TEXT = "Update";
    static final String EDIT_ACCOUNT_WINDOW_TITLE = "Edit Account";
    // The account object that gets edited
    private Account editedAccount;
    // The submit Button
    @FXML
    private Button submitButton;
    // The cancel Button
    @FXML
    private Button cancelButton;
    // The name text field
    @FXML
    private TextField nameField;
    // The host text field
    @FXML
    private TextField hostField;
    // The username text field
    @FXML
    private TextField usernameField;
    // The password text field
    @FXML
    private TextField passwordField;
    // The warning label
    @FXML
    private Label warningLabel;
    // The FXMLDocumentController of the main frame
    private final FXMLDocumentController fxmlcontr;
    private final ApplicationLogicIF logic;

    /**
     * The constructor of the HistoryFrameController which initializes the
     * constructor of the main frame
     *
     * @param logic - the application logic used by the main frame
     * @param fxmlcontr - the controller of the main frame
     */
    public EditAccountFrameController(final ApplicationLogicIF logic, FXMLDocumentController fxmlcontr) {
        this.logic = logic;
        this.fxmlcontr = fxmlcontr;
    }

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(final URL url, final ResourceBundle rb) {
        submitButton.setOnAction((event) -> pressedSubmit());
        cancelButton.setOnAction((event) -> pressedCancel());
        editedAccount = null;
        warningLabel.setOpacity(0.);
    }

    /**
     * This method closes the window.
     */
    public void close() {
        Stage window = (Stage) submitButton.getScene().getWindow();
        window.close();
    }

    /**
     * This method gets called when the ok button is pushed.
     */
    private void pressedSubmit() {
        boolean fieldIsEmpty = nameField.getText().isEmpty()
                || hostField.getText().isEmpty()
                || usernameField.getText().isEmpty()
                || passwordField.getText().isEmpty();
        if (fieldIsEmpty) {
            warningLabel.setOpacity(1.);
            return;
        }
        Account account = getAccountData();
        if (editedAccount != null) {
            logic.updateAccount(account);
        } else {
            logic.saveAccount(account);
        }
        close();
        fxmlcontr.updateAccountMenu();
    }

    /**
     * Fills the account object from editedAccount with the new data or if no
     * editedAccount object exists creates a new object with the new data.
     *
     * @return an Account object with the data from the text fields
     */
    private Account getAccountData() {
        Account account = editedAccount;
        if (account == null) {
            account = new Account();
        }
        account.setName(nameField.getText());
        account.setHost(hostField.getText());
        account.setUsername(usernameField.getText());
        account.setPassword(passwordField.getText());
        return account;
    }

    /**
     * This method gets called when the cancel button is pushed.
     */
    private void pressedCancel() {
        close();
    }

    /**
     * Changes this dialog from a "New Account" dialog to an "Edit Account"
     * dialog.
     *
     * @param name The name of the account that will be edited
     */
    public void changeToEdit(String name) {
        Account account = logic.getAccount(name);
        if (account == null) {
            close();
        }
        nameField.setText(account.getName());
        nameField.setEditable(false);
        nameField.setDisable(true);
        hostField.setText(account.getHost());
        usernameField.setText(account.getUsername());
        passwordField.setText(account.getPassword());
        submitButton.setText(UPDATE_BUTTON_TEXT);
        Stage window = (Stage) submitButton.getScene().getWindow();
        window.setTitle(EDIT_ACCOUNT_WINDOW_TITLE);
        editedAccount = account;
    }
}
