package de.beuth.fpa.mailApp.s812610s791836.applicationLogic.imap;

import de.beuth.fpa.mailApp.s812610s791836.applicationLogic.FolderManagerIF;
import de.beuth.fpa.mailApp.s812610s791836.applicationLogic.data.Account;
import de.beuth.fpa.mailApp.s812610s791836.applicationLogic.data.Folder;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import javax.mail.Store;

/**
 * This class manages folder objects from imap account.
 *
 * @author Thesa
 */
public class IMapFolderManager implements FolderManagerIF {

    //the used store obejct
    final Store store;

    // the root folder
    Folder topFolder;

    // the account
    final Account account;

    //special folder names for gmail accounts
    String special = "[Gmail]";

    public IMapFolderManager(final Account acc) {
        this.account = acc;
        this.store = IMapConnectionHelper.connect(acc);
        if (this.store != null) {
            this.configureTopFolder();
            this.loadContent(topFolder);
        }
    }

    @Override
    public Folder getTopFolder() {
        return topFolder;
    }

    @Override
    public void loadContent(final Folder f) {
        if (!f.getComponents().isEmpty()) {
            return;
        }
        try {
            final javax.mail.Folder jfolder = store.getFolder(f.getPath());
            for (final javax.mail.Folder ff : jfolder.list()) {
                if (ff.getName().equals(special)) {
                    for (final javax.mail.Folder gf : ff.list()) {
                        final Folder gfolder = convertFolder(gf);
                        if (gfolder != null) {
                            f.addComponent(gfolder);
                        }
                    }
                } else {
                    final Folder folder = convertFolder(ff);
                    if (folder != null) {
                        f.addComponent(folder);
                    }
                }
            }
        } catch (MessagingException ex) {
            Logger.getLogger(IMapFolderManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void configureTopFolder() {
        Folder folder = null;
        try {
            final javax.mail.Folder top = store.getDefaultFolder();
            final javax.mail.Folder[] subFolder = top.list();
            if (subFolder.length == 0) {
                folder = new Folder(false);
            } else {
                folder = new Folder(true);
            }
            folder.setName(account.getName());
            folder.setPath(top.getFullName());
        } catch (MessagingException ex) {
            Logger.getLogger(IMapFolderManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        topFolder = folder;
    }

    private Folder convertFolder(final javax.mail.Folder f) {
        Folder folder = null;
        try {
            final javax.mail.Folder sub[] = f.list();
            if (sub.length == 0) {
                folder = new Folder(false);
            } else {
                folder = new Folder(true);
            }
            folder.setName(f.getName());
            folder.setPath(f.getFullName());
        } catch (MessagingException ex) {
            Logger.getLogger(IMapFolderManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return folder;
    }
}
