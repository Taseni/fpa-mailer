package de.beuth.fpa.mailApp.s812610s791836.applicationLogic.imap;

import de.beuth.fpa.mailApp.s812610s791836.applicationLogic.EmailManagerIF;
import de.beuth.fpa.mailApp.s812610s791836.applicationLogic.data.Account;
import de.beuth.fpa.mailApp.s812610s791836.applicationLogic.data.Email;
import de.beuth.fpa.mailApp.s812610s791836.applicationLogic.data.Folder;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Store;

/**
 * This class manages emails from a imap account.
 *
 * @author Thesa
 */
public class IMapEmailManager implements EmailManagerIF {

    //the used store obejct
    final Store store;

    public IMapEmailManager(final Account acc) {
        store = IMapConnectionHelper.connect(acc);
    }

    @Override
    public void loadEmails(final Folder f) {
        if (f == null) {
            throw new RuntimeException("f must not be null");
        }
        if (f.getEmailsLoaded()) {
            return;
        }
        f.setEmailsLoaded();
        try {
            final javax.mail.Folder folder = store.getFolder(f.getPath());
            folder.open(javax.mail.Folder.READ_ONLY);
            final Message[] messages = folder.getMessages();
            if (messages == null || messages.length == 0) {
                return;
            }
            for (final Message m : messages) {
                final Email email = IMapEmailConverter.convertMessage(m);
                f.addEmail(email);
            }
            folder.close(true);
        } catch (MessagingException e) {
            Logger.getLogger(IMapEmailManager.class.getName()).log(Level.SEVERE, null, e);
        }

    }

}
