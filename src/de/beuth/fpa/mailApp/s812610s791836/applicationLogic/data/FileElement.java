package de.beuth.fpa.mailApp.s812610s791836.applicationLogic.data;

import java.io.File;

/**
 * This is the leaf part of a composite pattern.
 *
 * @author Simone Strippgen
 */
public class FileElement extends Component {

    /**
     * This constructor creates a FileElement.
     *
     * @param path File that should be represented as FileElement
     */
    public FileElement(File path) {
        super(path);
    }

    @Override
    public boolean isExpandable() {
        return false;
    }
}
