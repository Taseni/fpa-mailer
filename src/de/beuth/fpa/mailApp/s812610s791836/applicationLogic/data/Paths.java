package de.beuth.fpa.mailApp.s812610s791836.applicationLogic.data;

/**
 * This class holds standard paths for the project.
 *
 * @author Thesa
 */
public class Paths {

    public static final String ROOT_PATH = "/home/tim/NetBeansProjects/Account/";
    public static final String FOLDER_ICON_PATH = "/de/beuth/fpa/mailApp/s812610s791836/applicationLogic/data/folder.png";
    public static final String FOLDER_GREY_ICON_PATH = "/de/beuth/fpa/mailApp/s812610s791836/applicationLogic/data/folder_grey.png";
    public static final String FILE_ICON_PATH = "/de/beuth/fpa/mailApp/s812610s791836/applicationLogic/data/page.png";
}
