package de.beuth.fpa.mailApp.s812610s791836.applicationLogic.data;

import java.io.File;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;

/**
 * This is the component part of a composite pattern.
 *
 * @author Simone Strippgen
 */
@Entity
@Inheritance
public abstract class Component implements Serializable {

    @Id
    @GeneratedValue
    private long id;
    // absolute directory path to this component
    private String path;
    // name of the component (without path)
    private String name;

    public Component() {
        this.path = null;
        this.name = null;
    }

    /**
     * This constructor creates a component from a file and sets its path and
     * name.
     *
     * @param path path of the file
     */
    public Component(final File path) {
        this.path = path.getAbsolutePath();
        this.name = path.getName();
    }

    /**
     * This method adds kind components to a component.
     *
     * @param comp component that should be added
     */
    public void addComponent(final Component comp) {
        throw new UnsupportedOperationException("Not supported.");
    }

    /**
     * This method returns all kind components of this component.
     *
     * @return list of all kind components
     */
    public List<Component> getComponents() {
        throw new UnsupportedOperationException("Not supported.");
    }

    /**
     * This abstract method returns if the component is expandable
     *
     * @return true if component is expandable
     */
    public abstract boolean isExpandable();

    /**
     * This method returns the name of this component
     *
     * @return name of component
     */
    public String getName() {
        return name;
    }

    /**
     * This method sets the name for the component
     *
     * @param name name of the component
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * This method sets the path for the component
     *
     * @param p path of the component
     */
    public void setPath(final String p) {
        path = p;
    }

    /**
     * This method returns the path of this component
     *
     * @return path of component
     */
    public String getPath() {
        return path;
    }

    @Override
    public String toString() {
        return name;
    }

}
