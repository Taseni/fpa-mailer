package de.beuth.fpa.mailApp.s812610s791836.applicationLogic.data;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Transient;

/**
 * This class represents a folder component.
 *
 * @author Simone Strippgen
 *
 */
@Entity
public class Folder extends Component {

    // a boolean variable for representing if folder is expandable what means that it is not empty
    private final boolean expandable;
    
    // list that holds the content of the folder
    @Transient
    private final transient ArrayList<Component> content;
    
    // list that holds the emails in the folder
    @Transient
    private final transient List<Email> emails;
    
    // indicates if the contained emails are loaded
    @Transient
    private transient boolean emailsLoaded;

    public Folder() {
        super();
        expandable = false;
        content = new ArrayList<Component>();
        emails = new ArrayList<Email>();
        emailsLoaded = false;
    }

    public Folder(boolean expandable) {
        super();
        this.expandable = expandable;
        content = new ArrayList<Component>();
        emails = new ArrayList<Email>();
        emailsLoaded = false;
    }

    /**
     * This constructor creates a folder component and initializes its two lists
     * and sets its expandable.
     *
     * @param path path of the folder
     * @param expandable true if the folder ist expandable
     */
    public Folder(File path, boolean expandable) {
        super(path);
        this.expandable = expandable;
        content = new ArrayList<Component>();
        emails = new ArrayList<Email>();
        emailsLoaded = false;
    }

    @Override
    public boolean isExpandable() {
        return expandable;
    }

    @Override
    public void addComponent(Component comp) {
        content.add(comp);
    }

    @Override
    public List<Component> getComponents() {
        return content;
    }

    /**
     * This method returns all email components of this component.
     *
     * @return list of all kind emails
     */
    public List<Email> getEmails() {
        return emails;
    }

    /**
     * This method adds email components to a component.
     *
     * @param message the email that should be added
     */
    public void addEmail(Email message) {
        emails.add(message);
    }

    /**
     * This method is to be called when the emails in this folder were added to
     * it.
     */
    public void setEmailsLoaded() {
        emailsLoaded = true;
    }

    /**
     * Returns true if the emails in this folder are already loaded.
     *
     * @return true if emails are loaded
     */
    public boolean getEmailsLoaded() {
        return emailsLoaded;
    }

    @Override
    public String toString() {
        int email_count = emails.size();
        if (emailsLoaded) {
            return super.toString() + String.format(" (%d)", email_count);
        } else {
            return super.toString();
        }
    }
}
