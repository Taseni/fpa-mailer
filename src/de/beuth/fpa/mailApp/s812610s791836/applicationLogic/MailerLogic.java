package de.beuth.fpa.mailApp.s812610s791836.applicationLogic;

import de.beuth.fpa.mailApp.s812610s791836.applicationLogic.imap.IMapEmailManager;
import de.beuth.fpa.mailApp.s812610s791836.applicationLogic.xml.FileManager;
import de.beuth.fpa.mailApp.s812610s791836.applicationLogic.xml.XMLEmailManager;
import de.beuth.fpa.mailApp.s812610s791836.applicationLogic.account.AccountManager;
import de.beuth.fpa.mailApp.s812610s791836.applicationLogic.account.AccountManagerIF;
import de.beuth.fpa.mailApp.s812610s791836.applicationLogic.data.Account;
import de.beuth.fpa.mailApp.s812610s791836.applicationLogic.data.Email;
import de.beuth.fpa.mailApp.s812610s791836.applicationLogic.data.Folder;
import de.beuth.fpa.mailApp.s812610s791836.applicationLogic.imap.IMapFolderManager;
import java.io.File;
import java.util.LinkedList;
import java.util.List;

/**
 * The facade for email, folder and account manager.
 *
 * @author Thesa
 */
public class MailerLogic implements ApplicationLogicIF {

    private Folder selectedFolder;

    private FolderManagerIF folderManager;
    private EmailManagerIF emailManager;
    private final AccountManagerIF accountManager;

    public MailerLogic(final File file) {
        folderManager = new FileManager(file);
        emailManager = new XMLEmailManager();
        accountManager = new AccountManager();
    }

    @Override
    public Folder getTopFolder() {
        return folderManager.getTopFolder();
    }

    @Override
    public void loadContent(final Folder folder) {
        folderManager.loadContent(folder);
    }

    @Override
    public void loadEmails(final Folder folder) {
        emailManager.loadEmails(folder);
        selectedFolder = folder;
    }

    @Override
    public List<Email> searchEmails(String pattern) {
        final List<Email> emails = selectedFolder.getEmails();
        if (pattern.isEmpty()) {
            return emails;
        }
        pattern = pattern.toLowerCase();
        final List<Email> result = new LinkedList<>();
        for (final Email email : emails) {
            if (email.getSubject().toLowerCase().contains(pattern)) {
                result.add(email);
            } else if (email.getText().toLowerCase().contains(pattern)) {
                result.add(email);
            } else if (email.getSent().toLowerCase().contains(pattern)) {
                result.add(email);
            } else if (email.getReceiver().toLowerCase().contains(pattern)) {
                result.add(email);
            } else if (email.getSender().toLowerCase().contains(pattern)) {
                result.add(email);
            }
        }
        return result;
    }

    @Override
    public void changeDirectory(final File file) {
        folderManager = new FileManager(file);
        emailManager = new XMLEmailManager();
    }

    @Override
    public void saveEmails(final File path, final Folder folder) {
        XMLEmailManager.saveEmails(path, folder);
    }

    @Override
    public void openAccount(final String name) {
        Account account = accountManager.getAccount(name);
        if (account != null) {
            folderManager = new IMapFolderManager(account);
            emailManager = new IMapEmailManager(account);
            if (folderManager.getTopFolder() == null) { //some accounts are on hard drive as xml
                folderManager = new FileManager(new File(account.getTop().getPath()));
                emailManager = new XMLEmailManager();
            }
        }
    }

    @Override
    public Account getAccount(final String name) {
        return accountManager.getAccount(name);
    }

    @Override
    public List<String> getAllAccounts() {
        List<Account> accounts = accountManager.getAllAccounts();
        List<String> accountNames = new LinkedList();
        for (Account acc : accounts) {
            accountNames.add(acc.getName());
        }
        return accountNames;
    }

    @Override
    public boolean saveAccount(Account account) {
        return accountManager.saveAccount(account);
    }

    @Override
    public void updateAccount(Account account) {
        accountManager.updateAccount(account);
    }

}
