package de.beuth.fpa.mailApp.s812610s791836.applicationLogic;

import de.beuth.fpa.mailApp.s812610s791836.applicationLogic.data.Folder;

/**
 * This interface represents a manager for email objects.
 *
 * @author tim
 */
public interface EmailManagerIF {

    /**
     * This method load emails from the given folder.
     *
     * @param f the folder
     */
    void loadEmails(Folder f);

}
