package de.beuth.fpa.mailApp.s812610s791836.applicationLogic.xml;

import de.beuth.fpa.mailApp.s812610s791836.applicationLogic.FolderManagerIF;
import de.beuth.fpa.mailApp.s812610s791836.applicationLogic.data.Component;
import de.beuth.fpa.mailApp.s812610s791836.applicationLogic.data.Folder;
import java.io.File;

/*
 * This class manages a hierarchy of folders and their content which is loaded 
 * from a given directory path and contains emails in .XML format.
 * 
 * @author Simone Strippgen
 */
public class FileManager implements FolderManagerIF {

    //top Folder of the managed hierarchy
    Folder topFolder;

    /**
     * Constructs a new FileManager object which manages a folder hierarchy,
     * where file contains the path to the top directory. The contained emails
     * of the directory file are loaded into the top folder.
     *
     * @param file File which points to the top directory
     */
    public FileManager(File file) {
        topFolder = new Folder(file, true);
        loadContent(topFolder);
    }

    /**
     * Loads all relevant contained subfolders in the directory path of a folder
     * object into the folder. If the folder already has content, it will be
     * removed and loaded again.
     *
     * @param f the folder into which the content of the corresponding directory
     * should be loaded
     */
    @Override
    public void loadContent(Folder f) {
        if (!f.getComponents().isEmpty()) {
            return;
        }
        File[] files = new File(f.getPath()).listFiles();
        if (files == null || files.length == 0) {
            return;
        }
        for (File file : files) {
            Component e = null;
            if (file.isDirectory()) {
                File[] content = file.listFiles((File current, String name) -> new File(current, name).isDirectory());
                if (content == null || content.length == 0) {
                    e = new Folder(file, false);
                } else {
                    e = new Folder(file, true);
                }
            }
            if (e != null) {
                f.addComponent(e);
            }
        }
    }

    @Override
    public Folder getTopFolder() {
        return topFolder;
    }

}
