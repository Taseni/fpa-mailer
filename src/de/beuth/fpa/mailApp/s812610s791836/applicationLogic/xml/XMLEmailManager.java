package de.beuth.fpa.mailApp.s812610s791836.applicationLogic.xml;

import de.beuth.fpa.mailApp.s812610s791836.applicationLogic.EmailManagerIF;
import de.beuth.fpa.mailApp.s812610s791836.applicationLogic.data.Email;
import de.beuth.fpa.mailApp.s812610s791836.applicationLogic.data.Folder;
import java.io.File;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

/**
 * The manager class for emails.
 *
 * @author tim
 */
public class XMLEmailManager implements EmailManagerIF {

    /**
     * Loads all relevant contained emails in the directory path of a folder
     * object into the folder. If the folder already has content, it will be
     * removed and loaded again.
     *
     * @param f the folder into which the content of the corresponding directory
     * should be loaded
     */
    @Override
    public void loadEmails(final Folder f) {
        if (f == null) {
            throw new RuntimeException("f must not be null");
        }
        if (f.getEmailsLoaded()) {
            return;
        }
        f.setEmailsLoaded();
        final File[] files = new File(f.getPath()).listFiles();
        if (files == null || files.length == 0) {
            return;
        }
        for (final File file : files) {
            if (file.isFile()) {
                final Email email = getEmailFromFile(file);
                if (email != null) {
                    f.addEmail(email);
                }
            }
        }
    }

    /**
     * This method creates an email object from a file. It returns null if the
     * file is not a valid email XML file.
     *
     * @param file The email XML file object.
     * @return An email object containing the data from the XML file.
     */
    private Email getEmailFromFile(final File file) {
        if (!file.getName().toLowerCase().endsWith(".xml")) {
            return null;
        }
        Email email;
        try {
            final JAXBContext jaxbContext = JAXBContext.newInstance(Email.class);
            final Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            email = (Email) jaxbUnmarshaller.unmarshal(file);
        } catch (final JAXBException e) {
            return null;
        }
        return email;
    }

    public static void saveEmails(final File file, final Folder folder) {
        try {
            final JAXBContext jaxbContext = JAXBContext.newInstance(Email.class);
            final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            int i = 0;
            for (Email email : folder.getEmails()) {
                final File path = new File(String.format("%s/%03d_mail.xml", file.getAbsolutePath(), i));
                jaxbMarshaller.marshal(email, path);
                i++;
            }
        } catch (final JAXBException e) {

        }

    }

}
