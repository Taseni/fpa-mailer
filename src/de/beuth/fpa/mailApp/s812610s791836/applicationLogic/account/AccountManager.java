package de.beuth.fpa.mailApp.s812610s791836.applicationLogic.account;

import de.beuth.fpa.mailApp.s812610s791836.applicationLogic.data.Account;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * This class manages e-mail accounts.
 *
 * @author tim
 */
public class AccountManager implements AccountManagerIF {

    private final Map<String, Account> accounts;
    private final AccountDAOIF accountDB;

    public AccountManager() {
        accounts = new HashMap();
        accountDB = new AccountDBDAO();
        final List<Account> accList = accountDB.getAllAccounts();
        for (final Account acc : accList) {
            accounts.put(acc.getName(), acc);
        }
    }

    @Override
    public Account getAccount(final String name) {
        return accounts.get(name);
    }

    @Override
    public List<Account> getAllAccounts() {
        final List<Account> accList = new LinkedList();
        accList.addAll(accounts.values());
        return accList;
    }

    @Override
    public boolean saveAccount(final Account acc) {
        if (accounts.containsKey(acc.getName())) {
            return false;
        }
        accounts.put(acc.getName(), acc);
        accountDB.saveAccount(acc);
        return true;
    }

    @Override
    public boolean updateAccount(final Account account) {
        accounts.put(account.getName(), account);
        return accountDB.updateAccount(account);
    }

}
