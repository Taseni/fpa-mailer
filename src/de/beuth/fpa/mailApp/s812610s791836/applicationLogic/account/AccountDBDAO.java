package de.beuth.fpa.mailApp.s812610s791836.applicationLogic.account;

import de.beuth.fpa.mailApp.s812610s791836.applicationLogic.data.Account;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 * This is the class for managing account data via database.
 *
 * @author Thesa
 */
public class AccountDBDAO implements AccountDAOIF {

    private final EntityManagerFactory emf;

    public AccountDBDAO() {
        TestDBDataProvider.createAccounts();
        emf = Persistence.createEntityManagerFactory("FPA-MailerPU");
    }

    @Override
    public List<Account> getAllAccounts() {
        final EntityManager em = emf.createEntityManager();
        final Query q = em.createQuery("select a from Account a");
        final List<Account> acclist = q.getResultList();
        em.close();
        return acclist;
    }

    @Override
    public Account saveAccount(Account acc) {
        final EntityManager em = emf.createEntityManager();
        final EntityTransaction trans = em.getTransaction();
        trans.begin();
        em.persist(acc);
        trans.commit();
        em.close();
        return acc;
    }

    @Override
    public boolean updateAccount(Account acc) {
        final EntityManager em = emf.createEntityManager();
        final EntityTransaction trans = em.getTransaction();
        trans.begin();
        em.merge(acc);
        trans.commit();
        em.close();
        return true;
    }

}
